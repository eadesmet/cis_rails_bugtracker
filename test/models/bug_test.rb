require 'test_helper'

class BugTest < ActiveSupport::TestCase
  setup do
    @bug = Bug.create(title: "My Title", description: "My description", issue_type: 2, priority: 1, status: 0)
  end
  
  test "title cannot be blank" do
    @bug.title = ""
    assert_not @bug.valid?
  end
  
  test "description cannot be blank" do
    @bug.description = ""
    assert_not @bug.valid?
  end
  
  test "issue_type must be valid" do
    invalid_issue_types = [-12, -1, 4, 12]
    invalid_issue_types.each do |iit|
      begin
        @bug.issue_type = iit
        assert false, "#{iit} should be invalid"
      rescue
        assert true
      end
    end
  end
  
  test "issue_type must be issue, enhancement, or feature" do
    valid_issue_type = [:issue, :enhancement, :feature]
    valid_issue_type.each do |vit|
      begin
        @post.issue_type = vit
        assert true
      rescue
        assert false, "#{vit} should be invalid"
      end
    end
  end
  
  
  test "priority must be valid" do
    invalid_priorities = [-12, -1, 4, 12]
    invalid_priorities.each do |ip|
      begin
        @bug.priority = ip
        assert false, "#{ip} should be invalid"
      rescue
        assert true
      end
    end
  end
  
  test "priority must be low, medium, or high" do
    valid_priorities = [:low, :medium, :high]
    valid_priorities.each do |vp|
      begin
        @post.priority = vp
        assert true
      rescue
        assert false, "#{vp} should be invalid"
      end
    end
  end
  
  
  test "status must be valid" do
    invalid_statuses = [-10, -1, 2, 10]
    invalid_statuses.each do |is|
      begin
        @post.status = is
        assert false, "#{is} should be invalid"
      rescue
        assert true
      end
    end
  end
  
  test "status must be open, closed, or monitor" do
    valid_statuses = [:open, :closed, :monitor]
    valid_statuses.each do |vs|
      begin
        @post.status = vs
        assert true
      rescue
        assert false, "#{vs} should be invalid"
      end
    end
  end
  
  
  
  
end
