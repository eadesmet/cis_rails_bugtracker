class AllBugs extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
        error: null,
        isLoaded: false,
        bugs: []
    };
  }

  componentDidMount(){
    // Dev environment: Cloud 9
    // fetch('https://cis-658-trick819.c9users.io/bugs.json')
    // Heroku:
    fetch('https://ead-rails-bugtracker.herokuapp.com/bugs.json')
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            bugs: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

    render(){
        const { error, isLoaded, bugs } = this.state;
        
        if (error) {
            return (<h1>Error: {error.message}</h1>)
        }
        else if (!isLoaded) {
            return (<h1>Loading...</h1>)
        }
        else {
            return(
              <div>
                <h1>This Bugs Table was created with React!</h1>
                <table className="table table-striped table-bordered">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Author</th>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Issue type</th>
                            <th scope="col">Priority</th>
                            <th scope="col">Status</th>
                            <th colSpan="3"></th>
                        </tr>
                    </thead>
                    <tbody>
                        {bugs.map(bug => (
                            <BugsTableRow bug={bug} />
                        ))}
                    </tbody>
                </table>
                
              </div>
            )
        }
        
    }
}

class BugsTableRow extends React.Component {
    render() {
        var bug = this.props.bug;
        return (
            <tr>
                <td>{bug.author == null ? 'NA' : bug.author.fname + ' ' + bug.author.lname}</td>
                <td>{bug.title}</td>
                <td>{bug.description}</td>
                <td>{bug.issue_type}</td>
                <td>{bug.priority}</td>
                <td>{bug.status}</td>
                
                <td><Button url={'/bugs/' + bug.id} className='btn btn-secondary' text='Show' /></td>
                <td><Button url={'/bugs/' + bug.id + '/edit'} className='btn btn-secondary' text='Edit' /></td>
                <td><a data-confirm="Are you sure?" class="btn btn-danger" rel="nofollow" data-method="delete" href={'/bugs/' + bug.id}>Destroy</a></td>
            </tr>
            
        )
        
    }
}

class Button extends React.Component {
    render() {
        return(
            <a href={this.props.url} className={this.props.className}>{this.props.text}</a>
        )
    }
}