# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

issue_type: has three possible values: issue, enhancement, feature.
priority: priority of the issue with three possible levels: low, medium, high.
status: the current status of the bug with three possible settings: open, closed,monitor.





1.Title and description cannot be blank whenever a bug is created or edited.
2.Issue_type, priority and status must have valid values (you should represent thesevalues as enumerated types.)
3.The default value of issue_type should be feature.
4.The default value of priority should be medium.
5.The default value of status should be open.



1. lname, fname, and email cannot be blank
2. email must be unique and a valid format for an email address
3. thumbnail must end in .jpg, .png, or .gif. ok to leave blank