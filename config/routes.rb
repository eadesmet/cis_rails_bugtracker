Rails.application.routes.draw do
  root 'home#index'
  get 'ajaxexample', action: :ajaxexample, controller: 'home'
  resources :authors
  resources :bugs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
